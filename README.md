# ChinChin
Documento para explicar funcionamiento base del site.

| Nombre de Tecnología | Versión |
| ---------- | ---------- |
| Laravel   | v7.11.0   |
| Composer   | v1.10.1   |


## Requerimientos del Sistema
- PHP >= 7.2.5
- PHP BCMath (Extensión)
- PHP Ctype (Extensión)
- PHP Fileinfo (Extensión)
- PHP JSON (Extensión)
- PHP Mbstring (Extensión)
- PHP OpenSSL (Extensión)
- PHP PDO (Extensión)
- PHP Tokenizer (Extensión)
- PHP XML (Extensión)
- Composer (Manejador de paquetes)
- Apache mod_rewrite

## Despliegue
### Servidor Local o Producción 
Pasos para instalar el site:

1. Instalar PHP y sus extensiones 
2. Habilitar `mod_rewrite` *Caso Apache*
3. Apuntar `document_root` del dominio a la ruta `public` del laravel.
4. Ejecutar comando `composer install` para instalar las librerías y paquetes PHP de Laravel.


### Probar en modo local con Laravel

1. Ejecutar comando `php artisan serve`
2. Abrir navegador web por default `localhost:{puerto-expuesto-en-el-terminal}`

## Skeleton del Proyecto 
```
app/  => carpeta donde se encuentran controladores y modelos.
    |- User.php => modelo ejemplo
    |-Http/
        |- Controllers/ => Controladores

bootstrap/
config/
database/

public/ => carpeta donde se encuentran los recursos estático del site (css,js,img)
    |-img/ => carpeta de imágenes ordenadas por nombre de páginas (home ...).
    |-css/ => estilos del site.
    |-js/ => javascripts del site.

resources/ => carpeta donde aloja las vistas de la página (views/home ...)
    |- views/
        |- home/ => carpeta quecontiene todas las paginas del sitio web.
        |- layouts/ => carpeta contiene la maquetación base y del sitio web.
        |- partials/ => elementos reutilizables (barra clientes, menú, etc)
            |- sections/ => secciones reutilizables (clientes)
            |- utils/ => componentes globales reutilizables (header del menu)

routes/ => carpeta donde se definen las rutas de Laravel (web.php)

storage/
test/

vendor/ => librerias y dependencias del proyecto laravel

.editoconfig
.env => archivo para configurar variables de entorno en el sistema operativo
.gitattributes
.gitignore
.styleci.yml
artisan
composer.json
package.json
phpunit.xml
server.php
webpack.mix.js
```

## Estructura de vistas
Las vistas están basadas en el modelo de vistas jerárquicas.
```
-main => contiene archivos globales y estructura base HTML
   |- website (incluye header y footer del website)
        |- home
        |- personas
        |- negocio
        |- comercio
        |- contacto
        |- terminos                           
```

### Modelo de Creación de Página básica
El siguiente archivo es un ejemplo básico de una nueva vista.
```
@extends('layouts.website') // contiene el header y footer del site
@section('title', 'Mi titulo personalizado')

@section("styles")
    // link o etiquetas style que contienen reglas css
@endsection

@section("hero-banner")
    // sección para todos los hero (banners principales)
@endsection

@section("content")
    // html que aparecerá dentro del contenido <body>
@endsection

@section("scripts-body")
    // scripts que se ejecutan al cargar el <body>
@endsection

@section("scripts-head")
    // scripts que se ejecutan al cargar el <head>
@endsection
```

### Modelo de Creación de ruta.
- Ingresar al archivo `route/web.php`

```
Route::get('/minuevapagina', function () {
    return view('home.minuevapagina'); //=> views/home/minuevapagina.blade.php
})->name("home"); // => nombre simplificado de ruta
```
- Agregar al menú ubicado en `views/partials/utils/header-website.blade.php`
- Agregar al footer en `views/partials/utils/footer-website.blade.php`