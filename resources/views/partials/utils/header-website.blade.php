<nav class="navbar navbar-expand-lg navbar-light bg-white">
    <div class="container">
        <!-- Brand -->
        <a class="navbar-brand" href="{{route('home')}}">
            <img width="140" src="{{asset('img/chinchin_logo.svg')}}" alt="...">
        </a>
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <!-- Toggler -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fe fe-x"></i>
            </button>
            <!-- Navigation -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{route('usuarios')}}" aria-expanded="false">
                    Usuarios
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('comercios')}}" aria-expanded="false">
                    Comercios
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('nosotros')}}" aria-expanded="false">
                    Nosotros
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('contacto')}}" aria-expanded="false">
                    Contacto
                    </a>
                </li>
            </ul>
            <!-- Button -->
            <a class="navbar-btn btn btn-sm btn-outline-success mr-4" href="https://app.pagochinchin.com/signup" target="_blank">
            Registro
            </a>
            <a class="navbar-btn btn btn-sm btn-success lift" href="https://app.pagochinchin.com/login" target="_blank">
            Login
            </a>
        </div>
    </div>
</nav>