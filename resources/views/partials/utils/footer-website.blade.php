<footer class="pt-8 pt-md-11">
  <div class="container">
    <div class="row pb-12">
      <div class="col-12 col-md-4 col-lg-3">  
        <!-- Brand -->
        <img src="{{asset('img/chinchin_logo.svg')}}" width="100" alt="..." class="footer-brand img-fluid mb-2">
        <!-- Social -->
        <ul class="list-unstyled list-inline list-social mb-6 mb-md-0">
          <li class="list-inline-item list-social-item mr-3">
            <a href="#!" class="text-decoration-none">
              <img src="{{asset('img/icons/social/instagram.svg')}}" class="list-social-icon" alt="...">
            </a>
          </li>
          <li class="list-inline-item list-social-item mr-3">
            <a href="#!" class="text-decoration-none">
              <img src="{{asset('img/icons/social/facebook.svg')}}" class="list-social-icon" alt="...">
            </a>
          </li>
          <li class="list-inline-item list-social-item mr-3">
            <a href="#!" class="text-decoration-none">
              <img src="{{asset('img/icons/social/twitter.svg')}}" class="list-social-icon" alt="...">
            </a>
          </li>
        </ul>
      </div>
      <div class="col-6 col-md-6 col-lg-4">    
        
        <!-- List -->
        <ul class="list-unstyled text-muted mb-0">
          <li class="mb-3">
            <a href="{{route('terminos')}}" class="text-reset">
              Términos y Condiciones
            </a>
          </li>
          <li>
            <a href="#!" class="text-reset">
              Políticas de Uso
            </a>
          </li>
          <li>
            <a href="#!" class="text-reset">
              Marco Regulatorio
            </a>
          </li>
          <li>
            <a href="#!" class="text-reset">
              Prevención y control de legitimación de capitales
            </a>
          </li>
        </ul>
      </div>
      <div class="col-6 col-md-6 col-lg-4">
       
        <!-- List -->
        <ul class="list-unstyled text-muted mb-6 mb-md-8 mb-lg-0">
          <li class="mb-3">
            <a href="#!" class="text-reset">
              Formulario
            </a>
          </li>
          <li class="mb-3">
            <a href="{{route('contacto')}}" class="text-reset">
              Contáctanos
            </a>
          </li>
          <li class="mb-3">
            <a href="#!" class="text-reset">
              Preguntas Frecuentes
            </a>
          </li>
        </ul>
      </div>
    </div> <!-- / .row -->
    <div class="row">
      <div class="col-12 col-md-4 col-lg-4"> 
        <img src="{{asset('img/icons/chinchin/icono_venezuela.png')}}" width="40" alt="..." class="footer-brand img-fluid mb-2">
        <span class="text-muted ml-3 text-uppercase">Hecho en Venezuela</span>
      </div>
    </div>
  </div> <!-- / .container -->
</footer>