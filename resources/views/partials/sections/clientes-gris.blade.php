@section('styles')
  @parent
  <link rel="stylesheet" href="{{asset('css/owl.carousel/owl.carousel.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/owl.carousel/owl.theme.default.min.css')}}">
@endsection

@section('content')
  @parent
  <section class="py-6 py-md-8 bg-gray-200">
    <div class="container">
      <div class="row pb-6 justify-content-center">
        <div class="col-12 col-md-10 text-center">
          <!-- Heading -->
          <h2 class="h1">
            Clientes que Confían en <span class="text-success">Nosotros</span>
          </h2>
        </div>
      </div> <!-- / .row -->
      <div class="owl-carousel carousel-gris-inverse" >
        <div class="img-fluid">
          <img src="/img/clientes/logo6_gris.png">
        </div>
         <div class="img-fluid">
            <img src="/img/clientes/logo-okmart-gris.png">
          </div>
        <div class="img-fluid">
          <img src="/img/clientes/logo4_gris.png">
        </div>
        <div class="img-fluid">
          <img src="/img/clientes/logo5_gris.png">
        </div>
      
        <div class="img-fluid">
          <img src="/img/clientes/logo2_gris.png">
        </div>
        <div class="img-fluid">
          <img src="/img/clientes/logo3_gris.png">
        </div>
     
        <div class="img-fluid">
          <img src="/img/clientes/logo1_gris.png">
        </div>
  
      </div>
    </div> <!-- / .container -->
  </section>
@endsection

@section('scripts-body')
  @parent
  <script src="{{asset('js/owl.carousel/owl.carousel.min.js')}}"></script>
  <script>
    $(document).ready(function(){
      $(".carousel-gris-inverse").owlCarousel({
        items:3,
        loop:true,
        margin:20,
        rtl:true,
        autoplay:true,
        autoplayTimeout:1000,
        autoplayHoverPause:true,
        responsive:{
            600:{
                items:6
            }
        }
      });
    });
  </script>
@endsection







