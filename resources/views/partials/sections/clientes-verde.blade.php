
@section('styles')
  @parent
  <link rel="stylesheet" href="{{asset('css/owl.carousel/owl.carousel.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/owl.carousel/owl.theme.default.min.css')}}">
@endsection

@section('content')
  @parent
  <section class="py-6 py-md-8 bg-success">
    <div class="container">
      <div class="row pb-6 justify-content-center">
        <div class="col-12 col-md-10 text-center">
          <!-- Heading -->
          <h2 class="h1">
             <span class="text-white">Nuestros Aliados Financieros</span>
          </h2>
        </div>
      </div> <!-- / .row -->
      <div class="owl-carousel carousel-verde" >
          <div class="img-fluid">
            <img src="/img/clientes/logo-insular.png">
          </div>
         
          <div class="img-fluid">
            <img src="/img/clientes/logo-amberescoin.png">
          </div>
      </div>
    </div> <!-- / .container -->
  </section>
@endsection

@section('scripts-body')
  @parent
  <script src="{{asset('js/owl.carousel/owl.carousel.min.js')}}"></script>
  <script>
    $(document).ready(function(){
      
      $(".carousel-verde").owlCarousel({
        items:3,
        loop:true, // habilitar si desean que repita el slider
        margin:20,
        autoplay:true,
        autoplayTimeout:1000,
        autoplayHoverPause:true,
        responsive:{
            600:{
                items:6
            }
        }   
      });
    });
  </script>
@endsection