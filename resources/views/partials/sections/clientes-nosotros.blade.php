<section class="py-6 py-md-8 bg-success">
  <div class="container">
    <div class="flickity-viewport" data-flickity='{"groupCells": 6,"imagesLoaded": true, "autoPlay": true, "pageDots": false, "prevNextButtons": false, "contain": true}'>
       <div class="col-4 col-md-3 col-lg-2">  
        <div class="img-fluid svg-shim text-white">
          <img src="/img/clientes/logo_nosotros_3.png">
        </div>
      </div>

      <div class="col-4 col-md-3 col-lg-2">  
        <div class="img-fluid svg-shim text-white">
          <img src="/img/clientes/logo_nosotros_2.png">
        </div>
      </div>
      <div class="col-4 col-md-3 col-lg-2">  
        <div class="img-fluid svg-shim text-white">
          <img src="/img/clientes/logo_nosotros_1.png">
        </div>
      </div>
      
     
      <div class="col-4 col-md-3 col-lg-2">  
        <div class="img-fluid svg-shim text-white">
          <img src="/img/clientes/logo_nosotros_4.png">
        </div>
      </div>
    </div>
  </div> <!-- / .container -->
</section>