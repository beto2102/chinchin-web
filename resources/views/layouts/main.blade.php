<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{asset('fonts/Feather/feather.css')}}">
        <link rel="stylesheet" href="{{asset('libs/aos/dist/aos.css')}}">
        <link rel="stylesheet" href="{{asset('libs/flickity-fade/flickity-fade.css')}}">
        <link rel="stylesheet" href="{{asset('libs/flickity/dist/flickity.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/theme.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/custom.all.css')}}">
        <link rel="stylesheet" href="{{asset('css/mobile.css')}}" screen="">
        @yield('styles')
        <title>@yield('title')</title>
        @yield('scripts-head')
    </head>
    <body>
        @yield('base')
        @yield('modal')
        <script src="{{asset('libs/jquery/dist/jquery.min.js')}}"></script>
        <script src="{{asset('libs/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{asset('libs/aos/dist/aos.js')}}"></script>
        <script src="{{asset('libs/flickity/dist/flickity.pkgd.min.js')}}"></script>
        <script src="{{asset('libs/flickity-fade/flickity-fade.js')}}"></script>
        <script src="{{asset('libs/imagesloaded/imagesloaded.pkgd.min.js')}}"></script>
        <script src="{{asset('js/theme.min.js')}}"></script>
        @yield('scripts-body')
    </body>
</html>
