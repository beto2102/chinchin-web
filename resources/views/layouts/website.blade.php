@extends('layouts.main')

@section('base')
    @include('partials.utils.header-website')
    @yield('hero-banner')
    @yield('content')
    @include('partials.utils.footer-website')
@endsection

