@extends('layouts.website')

@section('title', 'Contacto ChinChin')

@section('styles')
    <link href='https://api.mapbox.com/mapbox-gl-js/v0.53.0/mapbox-gl.css' rel='stylesheet' />
@endsection

@section('hero-banner')
    <section class="position-relative py-8 pt-md-11 mb-9">
 
        <!-- Contetn -->
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-md-6 order-md-2">
                
                <!-- Image -->
                <div class="mb-8 mb-md-0">
                    <img src="{{asset('img/contacto/chinchin_contacto_hero_1.png')}}" alt="..." class="img-fluid mw-md-110"  data-aos="fade-rigth">
                </div>

                </div>
                <div class="col-12 col-md-6 order-md-1 text-center text-md-left" data-aos="fade-up">
                
                <!-- Heading -->
                <h1 class="display-3 font-weigth-normal text-success">
                    Contáctanos
                </h1>

                <!-- Text -->
                <p class="lead text-muted text-justify mb-6 mb-md-8">
                Tu opinión es muy importante para nosotros, utiliza los diferentes canales que ponemos a tu disposición para que te comuniques con nuestro equipo.
                </p>

                

                </div>
            </div> <!-- / .row -->
            <div class="row mb-7 mb-md-9 pt-8">
                <div class="col-12 col-md-6 text-center">
                    
                    <!-- Heading -->
                    <h6 class="text-gray-700 mb-1">
                        Comunícate con nosotros mediante el Centro de Atención Telefónica de <span class="text-uppercase">soluciones financieras chinchin, c.a</span> a tráves del número
                    </h6>

                    <!-- Link -->
                    <div class="mb-5 mb-md-0">
                        <a href="tel:02126888067" class="h2 text-success">
                            (0212) 688-8067
                        </a>    
                    </div>

                </div>
                <div class="col-12 col-md-6 text-center border-left-md border-gray-300">
                    
                    <!-- Heading -->
                    <h6 class="text-gray-700 mb-1">
                        Escríbenos un correo detallado a 
                    </h6>

                    <!-- Link -->
                    <div class="mb-5 mb-md-0">
                    <a href="mailto:atcliente@pagochinchin.com" class="h2 text-success">
                        atcliente@pagochinchin.com
                    </a>
                    </div>

                </div>
            </div> <!-- / .row -->
        </div> <!-- / .container -->

    </section>
@endsection

@section('content')
    {{-- RESPUESTA --}}
    <section class="pt-4">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-md-6 col-lg-7">
                    <!-- Screenshot -->
                    <div class="mb-8 mb-md-0">
                    <!-- Image -->
                    <img src="{{asset('img/contacto/chinchin_contacto_respuesta.png')}}" alt="..." class="img-fluid mw-md-110 float-right mr-md-6 mb-6 mb-md-0">
                    </div>
                </div>
                <div class="col-12 text-left col-md-6 col-lg-5">
                    <!-- Badge -->
                    <div class="text-md-left text-center">

                    <span class="badge badge-pill badge-success-soft mb-3">
                    <span class="h5 text-uppercase">respuesta</span>
                    </span>
                    </div>
                    <!-- Heading -->
                    <h2 class="h1 font-weight-bold">
                        En <span class="text-success text-uppercase">CHINCHIN</span> atenderte es nuestra prioridad
                    </h2>

                    <!-- Text -->
                    <p class="text-gray-700 text-justify">
                        Nuestro equipo de trabajo esta capacitado y comprometido con tu bienestar, ofreciéndote atención personalizada y tecnologia de vanguardia.
                    </p>
                    <p class="text-gray-700 text-justify">Desde el momento en que te conviertes en un Usuario Verificado o Cliente Afiliado a <span class="text-uppercase text-success">CHINCHIN</span> empiezas a ser parte de nuestra familia digital.</p>

                </div>
            </div> <!-- / .row -->
        </div> <!-- / .container -->
    </section>
    {{-- FORMULARIO DE CONTACTO --}}
    <section class="pt-8 pt-md-11 pb-8 pb-md-14">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-12 col-md-10 col-lg-8 text-center">

              <span class="badge badge-pill badge-success-soft mb-3">
              <span class="h5 text-uppercase">formulario</span>
              </span>
            <!-- Heading -->
            <h2 class="h1 font-weight-bold">
             Envíanos tus inquietudes y en breve te responderemos.
            </h2>

            <!-- Text -->
            <p class="font-size-lg text-muted mb-7 mb-md-9">
              Rellena el formulario con tu información y haz click en el botón ''Enviar mensaje''.
            </p>

          </div>
        </div> <!-- / .row -->
        <div class="row justify-content-center">
          <div class="col-12 col-md-12 col-lg-10">
            
            <!-- Form -->
            <form>
              <div class="row">
                <div class="col-12 col-md-6">
                  <div class="form-group mb-5">
                    
                    <!-- Label -->
                    <label for="contactName">
                      Nombre Completo
                    </label>

                    <!-- Input -->
                    <input type="text" class="form-control" id="contactName" placeholder="Nombre Completo">

                  </div>
                </div>
                <div class="col-12 col-md-6">
                  <div class="form-group mb-5">
                    
                    <!-- Label -->
                    <label for="contactEmail">
                      Email
                    </label>

                    <!-- Input -->
                    <input type="email" class="form-control" id="contactEmail" placeholder="Email">

                  </div>
                </div>
              </div> <!-- / .row -->
              <div class="row">
                <div class="col-12">
                  <div class="form-group mb-7 mb-md-9">
                    
                    <!-- Label -->
                    <label for="contactMessage">
                      ¿En qué podemos ayudarte?
                    </label>

                    <!-- Input -->
                    <textarea class="form-control" id="contactMessage" rows="5" placeholder="Mensaje"></textarea>

                  </div>
                </div>
              </div> <!-- / .row -->
              <div class="row justify-content-center">
                <div class="col-auto">
                  <div class="g-recaptcha mb-2" id="html_element"></div>
                  <!-- Submit -->
                  <button type="submit" class="btn btn-success lift">
                    Enviar Mensaje
                  </button>

                </div>
              </div> <!-- / .row -->
            </form>

          </div>
        </div> <!-- / .row -->
      </div> <!-- / .container -->
    </section>

       {{-- FORMULARIO DE CONTACTO --}}
    <section class="pt-8 pt-md-11 ">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-12 text-center">

              <span class="badge badge-pill badge-success-soft mb-3">
              <span class="h5 text-uppercase">dirección</span>
              </span>
            <!-- Heading -->
            <h2 class="h1 font-weight-bold">
            Si quieres ser atendido de manera personalizada, visítanos en nuestra <span class="text-success">Oficina Principal</span>
            </h2>

          </div>
        </div> <!-- / .row -->
        
      </div> <!-- / .container -->
    </section>
    <section class="pb-8">
      <div class="container-fluid">
         <div class="row">
           <div class="col-12 col-md-6 col-lg-5 offset-lg-1 py-8 py-md-11 py-lg-12 position-relative order-2 order-md-1">
            
            <!-- Heading -->
            <h2 class="h4">
             Dirección
            </h2>

            <!-- Text -->
            <p class="font-size-lg text-justify text-gray-700 mb-6">
              Av Francisco de Miranda, Torre Europa, Piso 8.
              Oficina 8-B-2 Urbanización Chacao, Caracas, Estado Miranda. República Bolivariana de Venezuela
            </p>
            <p class="font-size-lg mb-6 text-justify">
             También puedes comunicarte con nosotros a través de nuestras redes sociales oficiales.
            </p>
            
            <ul class="text-success list-unstyled list-inline list-social mb-6 mb-md-0">
            <li class="list-inline-item list-social-item mr-3">
              <a href="#!" class="text-decoration-none">
                <img src="{{asset('img/icons/social/instagram.svg')}}" class="list-social-icon" alt="...">
              </a>
            </li>
            <li class="list-inline-item list-social-item mr-3">
              <a href="#!" class="text-decoration-none">
                <img src="{{asset('img/icons/social/twitter.svg')}}" class="list-social-icon" alt="...">
              </a>
            </li>
            <li class="list-inline-item list-social-item mr-3">
              <a href="#!" class="text-gray-700 text-decoration-none">
              @pagochinchin
              </a>
            </li>
          </ul>


          </div>
          <div class="col-12 col-md-6 px-0 bg-gray-200 position-relative order-1 order-md-2">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3923.1151324821108!2d-66.8634706!3d10.4915894!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8c2a58fdafcdd757%3A0xd2bf02cc07ec0ce7!2sTorre%20Europa%2C%20Avenida%20Francisco%20de%20Miranda%2C%20Caracas%201060%2C%20Miranda!5e0!3m2!1ses-419!2sve!4v1590367323444!5m2!1ses-419!2sve" class="w-100 h-100" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
          </div>
        </div> <!-- / .row -->
      </div>
    </section>
@endsection

@section('scripts-body')
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
    async defer>
    </script>
    <script type="text/javascript">
  var onloadCallback = function() {
     grecaptcha.render('html_element', {
          'sitekey' : '6LctH_wUAAAAAEjIgH8cgal-t3__FwHmdVht1RMY'
        });
  };
</script>
@endsection