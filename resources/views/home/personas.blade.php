@extends('layouts.website')

@section('title', 'Personas ChinChin')

@section('hero-banner')
    <section class="position-relative pt-12 pt-md-14 mt-n11">
      <!-- Content -->
      <div class="container">
        <div class="row align-items-center text-center text-md-left">
          <div class="col-12 col-md-5">
            
            <!-- Image -->
            <img src="{{asset('img/personas/chinchin_personas_hero_1.png')}}" alt="..." class="img-fluid mw-md-120 float-md-right mb-6 mb-md-0" data-aos="fade-right">

          </div>
          <div class="col-12 col-md-6 offset-md-1">
            
            <!-- Heading -->
            <h1 class="display-4 text-center text-md-left font-weight-normal">
               Nuestras soluciones se adaptan a cualquier <span class="text-success">Persona</span>

            </h1>
            <div class="d-flex">
            <!-- Badge -->
            <div class="badge badge-lg badge-rounded-circle badge-success-soft mt-1">
              <i class="fe fe-check"></i>
            </div>
            <!-- Body -->
            <div class="ml-5">
              <!-- Heading -->
              <h3 class="text-left lead font-weight-bold">
               Wallet Digital <span class="text-success">Multimoneda</span>
              </h3>
              <!-- Text -->
              <p class="lead text-left text-gray-700 mb-6">
                Usa tu billetera virtual para enviar, recibir y pagar con diferentes divisas y criptomonedas.
              </p>
            </div>
          </div>
          <div class="d-flex">   
            <!-- Badge -->
            <div class="badge badge-lg badge-rounded-circle badge-success-soft mt-1">
              <i class="fe fe-check"></i>
            </div>
            <!-- Body -->
            <div class="ml-5">
              <!-- Heading -->
              <h3 class="text-left lead font-weight-bold">
                Puntos CHINCHIN
              </h3>
              <!-- Text -->
              <p class="lead text-left text-gray-700 mb-6">
                Paga en cualquier comercio afiliado a nuestro ecosistema financiero.

              </p>
            </div>
          </div>

          </div>
        </div> <!-- / .row -->
      </div> <!-- / .container -->

    </section>
@endsection

@section('content')
  {{-- SECTION PROCESO --}}
  <section class="pt-8 pt-md-11">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-12 text-center">
          <!-- Badge -->
          <span class="badge badge-pill badge-success-soft mb-3">
            <span class="h5 text-uppercase">divisas</span>
          </span>
          <!-- Heading -->
          <h2 class="h1">
          Realiza todas tus Transacciones Financieras de manera simple, rápida y segura con <span class="text-success">CHINCHIN</span>

          </h2>
          <!-- Text -->
          <p class="lead text-gray-700 mb-7 mb-md-9">Simplificamos tu vida con una plataforma amigable que se adapta a tus necesidades como <span class="text-success">Venezolano</span>.</p>
        </div>
      </div> <!-- / .row -->
          <div class="row">
          <div class="col-12 col-md-6 col-lg-4 text-center mb-6">

            <!-- Icon -->
            <div class="icon icon-lg mb-4">
             <img width="200" src="{{asset('img/personas/chinchin_personas_bolivares.png')}}" alt="bolivares">
            </div>

            <!-- Heading -->
            <h3 class="font-weight-bold">
              En Moneda Nacional
            </h3>

            <!-- Text -->
            <p class="text-muted mb-8">
               Envía, recibe y paga en Bolívares.
            </p>

          </div>
          <div class="col-12 col-md-6 col-lg-4 text-center mb-6">

            
           <!-- Icon -->
            <div class="icon icon-lg mb-4">
             <img width="200" src="{{asset('img/personas/chinchin_personas_divisa_internacional.png')}}" alt="bolivares">
            </div>

            <!-- Heading -->
            <h3 class="font-weight-bold">
              Monedas Internacionales
            </h3>

            <!-- Text -->
            <p class="text-muted mb-8 mb-lg-0">
              Envia, recibe y paga en Dólares y Euros.
            </p>

          </div>
          <div class="col-12 col-md-6 col-lg-4 text-center mb-6">

            <!-- Icon -->
            <div class="icon icon-lg mb-4">
             <img width="200" src="{{asset('img/personas/chinchin_personas_criptomonedas.png')}}" alt="bolivares">
            </div>

            <!-- Heading -->
            <h3 class="font-weight-bold">
              Criptomonedas
            </h3>

            <!-- Text -->
            <p class="text-muted mb-8 mb-md-0">
              Envía, recibe, compra y paga con cualquier Criptomoneda.
            </p>

          </div>
        </div> <!-- / .row -->
    </div> <!-- / .container -->
  </section>


    {{-- SECTION PROCESO --}}
  <section class="py-8 pt-md-11">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-12 col-md-10 text-center">
          <!-- Badge -->
          <span class="badge badge-pill badge-success-soft mb-3">
            <span class="h5 text-uppercase">wallet</span>
          </span>
          <!-- Heading -->
          <h2 class="h1">
            Con tu <span class="text-success">Wallet Multimoneda</span> puedes
          </h2>
          <!-- Text -->
          <p class="lead text-gray-700 mb-7 mb-md-9">Realizar cualquier transacción en la divisa o criptomoneda que necesites al momento.</p>
        </div>
      </div> <!-- / .row -->
      <div class="row align-items-center">
        <div class="col-12 col-md-6 col-lg-7">
          <!-- Screenshot -->
          <div class="mb-8 mb-md-0">
            <!-- Image -->
            <img src="{{asset('img/personas/chinchin_personas_proceso.png')}}" alt="..." class="img-fluid mw-md-110 float-right mr-md-6 mb-6 mb-md-0">
          </div>
        </div>
        <div class="col-12 col-md-6 col-lg-5">
          <!-- List -->
          <div class="d-flex mb-6">
            <!-- Badge -->
            <div style="font-size: 100%;" class="badge badge-lg badge-rounded-circle badge-success-soft mt-1">
              <span>1</span>
            </div>
            <!-- Body -->
            <div class="ml-5">
              <!-- Heading -->
              <h4 class="lead">
                Depósito y retiros en Dólares y Euros
              </h4>
             
            </div>
          </div>
          <div class="d-flex mb-6">   
            <!-- Badge -->
            <div style="font-size: 100%;" class="badge badge-lg badge-rounded-circle badge-success-soft mt-1">
              <span>2</span>
            </div>
            <!-- Body -->
            <div class="ml-5">
              <!-- Heading -->
              <h4 class="lead">
                Transferencias bancarias en Bolívares
              </h4>
             
            </div>
          </div>
          <div class="d-flex mb-6">    
            <!-- Badge -->
            <div style="font-size: 100%;" class="badge badge-lg badge-rounded-circle badge-success-soft mt-1">
              <span>3</span>
            </div>
            <!-- Body -->
            <div class="ml-5">
              <!-- Heading -->
              <h4 class="lead">
                Depósitos y retiros en Criptomonedas
              </h4>
             
            </div>
          </div>
           <div class="d-flex mb-6">    
            <!-- Badge -->
            <div style="font-size: 100%;" class="badge badge-lg badge-rounded-circle badge-success-soft mt-1">
              <span>4</span>
            </div>
            <!-- Body -->
            <div class="ml-5">
              <!-- Heading -->
              <h4 class="lead">
                Transferencias entre Usuarios <span class="text-success text-uppercase">chinchin</span>
              </h4>
             
            </div>
          </div>
           <div class="d-flex mb-6">    
            <!-- Badge -->
            <div style="font-size: 100%;" class="badge badge-lg badge-rounded-circle badge-success-soft mt-1">
              <span>5</span>
            </div>
            <!-- Body -->
            <div class="ml-5">
              <!-- Heading -->
              <h4 class="lead">
                Paga en todos los puntos <span class="text-success text-uppercase">chinchin</span>
              </h4>
             
            </div>
          </div>
        </div>
      </div> <!-- / .row -->
    </div> <!-- / .container -->
  </section>

  {{-- SECTION SERVICIO MENSAJERIA DE TEXTO --}}
  <section class="py-8 pt-md-11">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12 col-md-7 col-lg-6 position-relative order-2 order-md-1 text-center text-md-left" data-aos="fade-right">
          <!-- Heading -->
         
          <span class="badge badge-pill badge-success-soft mb-3">
            <span class="h5 text-uppercase">términos</span>
          </span>
          <h2 class="h1">
            Límites
          </h2>
          <!-- Text -->
          <p class="font-size-lg text-justify text-muted mb-6">
            En <span class="text-success text-uppercase">chinchin</span> te ofrecemos la posibilidad de personalizar tus límites, nos adaptamos a tus necesidades.  Disfruta de una mayor libertad financiera al usar nuestros servicios.
          </p>
          
        </div>
        <div class="col-12 col-md-5 col-lg-6 position-relative order-1 order-md-2 mb-4">
          <div class="w-md-150 w-lg-130" data-aos="fade-left">
            <!-- Image -->
            <img src="{{asset('img/personas/chinchin_personas_limites.png')}}" class="img-fluid" alt="...">
          </div>
        </div>
      </div> <!-- / .row -->
    </div> <!-- / .container -->
  </section>

  <section class="py-8 py-md-13">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-12 col-md-10 text-center">

            <span class="badge badge-pill badge-success-soft mb-3">
          <span class="h5 text-uppercase">canales</span>
        </span>

          <!-- Heading -->
          <h2 class="h1 font-weight-bold">
            <span class="text-success">CHINCHIN</span> te facilita el acceso a los Medios y Canales de Pago más Innovadores del Mercado
          </h2>

          <!-- Text -->
          <p class="font-size-lg text-muted mb-6">
            Facilitamos e incluimos a nuestros usuario en los sistemas financieros del futuro
          </p>

          <p class="font-size-lg text-muted mb-9">Nuestra plataforma tecnológica cuenta con los más altos estándares para realizar sus transacciones con altos niveles de seguridad y calidad.</p>

        </div>
      </div> <!-- / .row -->
      <div class="row">
        <div class="col-12 col-md-6 col-lg-4 text-center mb-6">

          <!-- Icon -->
          <div class="icon icon-lg mb-4">
            <img width="100" src="{{asset('img/icons/chinchin/sms_verde.png')}}" alt="bolivares">
          </div>

          <!-- Heading -->
          <h3 class="font-weight-bold">
            Mensajes de Texto (SMS)
          </h3>

          <!-- Text -->
          <p class="text-muted mb-8">
            Envíe y reciba pagos sin la necesidad de un teléfono inteligente.
          </p>

        </div>
        <div class="col-12 col-md-6 col-lg-4 text-center mb-6">

          <!-- Icon -->
          <div class="icon icon-lg mb-4">
            <img width="100" src="{{asset('img/icons/chinchin/pago_movil_verde.png')}}" alt="bolivares">
          </div>

          <!-- Heading -->
          <h3 class="font-weight-bold">
            Pago Móvil Interbancario
          </h3>

          <!-- Text -->
          <p class="text-muted mb-8 mb-lg-0">
            También podrá hacer uso de su Pago Móvil dentro de nuestra plataforma.
          </p>

        </div>
        <div class="col-12 col-md-6 col-lg-4 text-center mb-6">

          <!-- Icon -->
          <div class="icon icon-lg mb-4">
            <img width="100" src="{{asset('img/icons/chinchin/btc_verde.png')}}" alt="bolivares">
          </div>

          <!-- Heading -->
          <h3 class="font-weight-bold">
            BITCOIN (BTC)
          </h3>

          <!-- Text -->
          <p class="text-muted mb-8 mb-md-0">
            Envíe y reciba Bitcoins con su Wallet multimoneda.
          </p>

        </div>
        <div class="col-12 col-md-6 col-lg-4 text-center mb-6">

          <!-- Icon -->
          <div class="icon icon-lg mb-4">
            <img width="100" src="{{asset('img/icons/chinchin/ptr_verde.png')}}" alt="bolivares">
          </div>

          <!-- Heading -->
          <h3 class="font-weight-bold">
            Criptomonedas
          </h3>

          <!-- Text -->
          <p class="text-muted mb-8 mb-lg-0">
            Convierta sus Petros en Bolívares fácil y rápido.
          </p>

        </div>
        <div class="col-12 col-md-6 col-lg-4 text-center mb-6">

          <!-- Icon -->
          <div class="icon icon-lg mb-4">
            <img width="100" src="{{asset('img/icons/chinchin/transferencia_bs_verde.png')}}" alt="bolivares">
          </div>

          <!-- Heading -->
          <h3 class="font-weight-bold">
            Transferencias Bancarias en BS
          </h3>

          <!-- Text -->
          <p class="text-muted mb-8 mb-md-0">
            Realice sus transferencias bancarias para adquirir Divisas y Criptomonedas.
          </p>

        </div>
        <div class="col-12 col-md-6 col-lg-4 text-center mb-6">

          <!-- Icon -->
          <div class="icon icon-lg mb-4">
            <img width="100" src="{{asset('img/icons/chinchin/dolares_euros_verde.png')}}" alt="bolivares">
          </div>

          <!-- Heading -->
          <h3 class="font-weight-bold">
            Dólares y Euros
          </h3>

          <!-- Text -->
          <p class="text-muted mb-0">
              Envía y recibe pagos en Dólares o Euros.
          </p>

        </div>
        <div class="col-12 mt-8 col-md-6 col-lg-4 text-center mb-6">

          <!-- Icon -->
          <div class="icon icon-lg mb-4">
            <img width="100" src="{{asset('img/icons/chinchin/liquidacion_verde.png')}}" alt="bolivares">
          </div>

          <!-- Heading -->
          <h3 class="font-weight-bold">
            Liquidación de BITCOIN
          </h3>

          <!-- Text -->
          <p class="text-muted mb-0">
              Haz uso de las liquidaciones de BTC con Chinchin
          </p>

        </div>
        <div class="col-12 mt-8 col-md-6 col-lg-4 text-center mb-6">

          <!-- Icon -->
          <div class="icon icon-lg mb-4">
            <img width="100" src="{{asset('img/icons/chinchin/punto_de_venta_2_verde.png')}}" alt="bolivares">
          </div>

          <!-- Heading -->
          <h3 class="font-weight-bold">
            Puntos de Venta
          </h3>

          <!-- Text -->
          <p class="text-muted mb-0">
              Adquiere tu punto de venta fijo o móvil de última generación.
          </p>

        </div>
        <div class="col-12 mt-8 col-md-6 col-lg-4 text-center mb-6">

          <!-- Icon -->
          <div class="icon icon-lg mb-4">
            <img width="100" src="{{asset('img/icons/chinchin/transacciones_interna_verde.png')}}" alt="bolivares">
          </div>

          <!-- Heading -->
          <h3 class="font-weight-bold">
            Transacciones Internas entre Usuarios
          </h3>

          <!-- Text -->
          <p class="text-muted mb-0">
              Realiza transacciones con otros usuarios de Chinchin dentro de la plataforma.

          </p>

        </div>
      </div> <!-- / .row -->
    </div> <!-- / .container -->
  </section>

  {{-- SECTION SOLUCIONES --}}
  <section class="py-8">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-12 col-lg-10 text-center">     
          <!-- Badge -->
          <span class="badge badge-pill badge-success-soft mb-3">
            <span class="h5 text-uppercase">seguridad</span>
          </span>
          <!-- Heading -->
          <h2 class="h1">
            Seguridad
          </h2>
          <!-- Text -->
          <p class="font-size-lg text-gray-700 mb-7 mb-md-9">
          Tu seguridad es una de nuestras prioridades, por eso contamos con protocolos tecnológicos que se ajustan a las mejores prácticas del sector.
          </p>
        </div>
      </div> <!-- / .row -->
      <div class="row align-items-center">
        <div class="col-md-6">
            <img src="{{asset('img/personas/chinchin_personas_seguridad.png')}}" class="img-fluid" alt="..."> 
        </div>
        <div class="col-md-6">
            <!-- List -->
          <div class="d-flex mt-8 mb-6">
            <!-- Icon -->
            <div class="icon text-primary">
              <img width="60" src="{{asset('img/icons/chinchin/encriptado.png')}}" alt="logo-solucion-1">
            </div>
            <div class="ml-5">
              <!-- Heading -->
              <h4 class="lead font-weight-bold">
                Encriptado
              </h4>
              <p class="text-gray-700 font-size-lg text-justify"> Contamos con un Sistema y Encriptado de última generación en seguridad digital.</p>
            </div>
          </div>
          <div class="d-flex mb-6">
            <!-- Icon -->
            <div class="icon text-primary">
              <img width="60" src="{{asset('img/icons/chinchin/doble_verificacion.png')}}" alt="logo-solucion-1">
            </div>
            <div class="ml-5">
              <!-- Heading -->
              <h4 class="lead font-weight-bold">
                Doble Verificación
              </h4>
              <p class="text-gray-700 font-size-lg text-justify">Añadimos una ''Capa Extra'' de seguridad con el sistema de Doble Verificación.</p>
            </div>
          </div>
          <div class="d-flex mb-6">
            <!-- Icon -->
            <div class="icon text-primary">
              <img width="60" src="{{asset('img/icons/chinchin/verificacion_usuario.png')}}" alt="logo-solucion-1">
            </div>
            <div class="ml-5">
              <!-- Heading -->
              <h4 class="lead font-weight-bold">
                Verificación de Usuario
              </h4>
              <p class="text-gray-700 font-size-lg text-justify">Nos aseguramos de que solo tú puedas acceder a tu cuenta. Tu seguridad es nuestra prioridad.</p>
            </div>
          </div>
        </div>
      </div>
    </div> <!-- / .container -->
  </section>

  @include('partials.sections.clientes-verde')


@endsection
