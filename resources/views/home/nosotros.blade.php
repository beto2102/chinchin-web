@extends('layouts.website')

@section('title', 'Nosotros ChinChin')

@section('hero-banner')
    <section class="position-relative pt-12 pt-md-14 mt-n11">
      <!-- Content -->
      <div class="container">
        <div class="row align-items-center text-center text-md-left">
          <div class="col-12 col-md-5 offset-md-1">
            
            <!-- Image -->
            <img src="{{asset('img/nosotros/chinchin_nosotros_hero_1.png')}}" alt="..." class="img-fluid mw-100 mw-md-130 float-md-right mb-6 mb-md-0" data-aos="fade-right">

          </div>
          <div class="col-12 col-md-6">
            
            <!-- Heading -->
            <h1 class="display-3 text-md-right">
                ¿Quienes <span class="text-success">Somos?</span>
            </h1>

            <!-- Text -->
            <p class="font-size-lg text-muted mb-0 text-justify" data-aos="fade-left" data-aos-delay="200">
             <span class="text-success text-uppercase">CHINCHIN</span> es una empresa FinTech fundada en el año 2019, conformada por profesionales venezolanos con amplia experiencia en el sector bancario, bursátil y tecnológico.
Nos especializamos en ofrecer nuevas alternativas tecnológicas y transaccionales dedicadas a innovar sobre los medios de pago tradicionales presentes en el mercado, con la finalidad de facilitar el movimiento del dinero de nuestros clientes en todas sus operaciones diarias, con bajos costos operativos, procesos automatizados, seguridad informática y confiabilidad.
            </p>

          </div>
        </div> <!-- / .row -->
      </div> <!-- / .container -->

    </section>
@endsection

@section('content')
    {{-- MISION --}}
  <section class="pt-12  pb-md-10 pb-lg-10 bg-white">
      <div class="container">

       <div class="row justify-content-center">
        <div class="col-12 text-center">
          <!-- Badge -->
          <span class="badge badge-pill badge-success-soft mb-3">
            <span class="h5 text-uppercase">nosotros</span>
          </span>
          <!-- Heading -->
          <h2 class="h1">
          Nuestro <span class="text-success">Objetivo</span> es lograr que todas las transacciones puedan ejecutarse de manera simple, rápida y segura.
          </h2>
          <!-- Text -->
          <p class="text-gray-700 mb-7 mb-md-9">Estamos convencidos de que el uso de la tecnología aplicada a los entornos financieros no es el futuro, sino el presente.</p>
        </div>
      </div> <!-- / .row -->
        <div class="row align-items-center justify-content-between">
          <div class="col-12 col-md-6 col-lg-5 position-relative order-2 order-md-1">
    

            <!-- Heading -->
            <h2 class="h1">
              Misión
            </h2>

            <!-- Text -->
            <p class="font-size-lg text-justify text-gray-700 mb-6">
            Somos una empresa proveedora de servicios financieros transaccionales que facilitan el acceso a medios de pago innovadores que ofrecen una solución a todas las necesidades y requerimientos del mercado venezolano, con infraestructura propia y aliados comerciales.
            </p>
                    <!-- Text -->
            <p class="font-size-lg text-gray-700 text-justify mb-6">
            Estamos construyendo la plataforma mas completa de <span class="text-success">Venezuela</span> para realizar transacciones de forma segura y simple.
             </p>

                    <!-- Text -->
            <p class="font-size-lg text-gray-700 mb-6 text-justify">
            Adoptamos una conducta responsable con nuestro entorno y creamos valor para nuestro país
             </p>
          
           
          </div>
          <div class="col-12 col-md-6 position-relative order-1 order-md-2">
            
            <!-- Images -->
          
                
            <img src="{{('img/nosotros/chinchin_nosotros_mision.png')}}" alt="..." class="img-fluid mb-4 rounded">


          </div>
        </div> <!-- / .row -->
      </div> <!-- / .container -->
    </section>
 <section class="pt-8 pt-md-12 pb-10 pb-md-10 pb-lg-10">
      <div class="container">
        <div class="row align-items-center justify-content-between">
          <div class="col-12 col-md-6">


            <img src="{{('img/nosotros/chinchin_nosotros_vision.png')}}" alt="..." class="img-fluid mb-4 rounded">


          </div>
          <div class="col-12 col-md-6 col-lg-5">
            
            <!-- Heading -->
            <h2 class="h1">
              Visión
            </h2>

            <!-- Text -->
            <p class="font-size-lg text-gray-700 mb-6 text-justify">
              Conectar a millones de personas y comercios a través de soluciones transaccionales que mejoren su vida. Ser una organización líder, útil y responsable con la sociedad, contribuyendo con su seguridad y desarrollando tecnología que genere valor a las operaciones financieras de nuestros clientes, enfocada en garantizar la plena satisfacción de sus usuarios y aliados estratégicos.
            </p>

          </div>
        </div> <!-- / .row -->
      </div> <!-- / .container -->
    </section>

    <!-- SUPPORT
    ================================================== -->
    <section class="pt-10 pt-lg-9 pb-md-q0 pb-lg-10 bg-white">
      <div class="container">
        <div class="row align-items-center justify-content-between">
          <div class="col-12 col-md-6 col-lg-5 position-relative order-2 order-md-1">
       
            <h2 class="h1 text-success text-uppercase">
                CHINCHIN
            </h2>

            <!-- Text -->
            <p class="font-size-lg text-gray-700 mb-6 text-justify">
             Es un ecosistema digital que ofrece un conjunto de servicios financieros transaccionales únicos, convirtiéndose en una opción en la que cualquier empresa puede tener acceso a los modelos de negocio mas modernos y escalables para su negocio.
            </p>
             
        
          </div>
          <div class="col-12 col-md-6 position-relative order-1 order-md-2">
            <img src="{{('img/nosotros/chinchin_nosotros_solucion_nacional.png')}}" alt="..." class="img-fluid mb-4 rounded">
            

          </div>
        </div> <!-- / .row -->
      </div> <!-- / .container -->
    </section>


  {{-- @include('partials.sections.clientes-nosotros') --}}
   @include('partials.sections.clientes-gris')
@endsection