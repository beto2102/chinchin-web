@extends('layouts.website')

@section('title', 'Comercios ChinChin')

@section('hero-banner')
    <section class="position-relative pt-12 pt-md-14 pb-md-12 mt-n11">
      <!-- Content -->
      <div class="container">
        <div class="row align-items-center text-center text-md-left">
          <div class="col-12 col-md-6">
            
            <!-- Image -->
            <img src="{{asset('img/negocio/chinchin_negocio_hero_1.png')}}" alt="..." class="img-fluid mw-md-110 float-md-right mb-6 mb-md-0" data-aos="fade-right">

          </div>
          <div class="col-12 col-md-6">
            
            <!-- Heading -->
            <h1 class="display-3 text-hero-mobile text-center text-md-left font-weight-normal">
               
              <div data-aos="fade-left">¿Tienes un negocio y<br>
              <span class="text-success" data-aos="fade-left" data-aos-delay="100">quieres que sea más rentable?</span></div>
            </h1>

            <!-- Text -->
            <p class="lead text-muted mb-0 text-justify" data-aos="fade-left" data-aos-delay="200">
             <span class="text-uppercase text-success">CHINCHIN</span> te ofrece un servicio en línea que te permite monitorear todas tus transacciones en tiempo real.
            </p>

          </div>
        </div> <!-- / .row -->
      </div> <!-- / .container -->

    </section>
@endsection


@section("content")
    {{-- DASHBOARD --}}
    <section class="pt-8 py-md-14">
        <div class="container">
            <div class="row align-items-center justify-content-between">
                <div class="col-12 col-md-6">         
                <!-- Image -->
                <div class="mb-10 mb-md-0">
                    <img src="{{asset('img/negocio/chinchin_negocio_dashboard.png')}}" alt="..." class="img-fluid" data-aos="fade-up">
                </div>
                </div>
                <div class="col-12 col-md-6 col-lg-5">
                <div class="text-md-left text-center">
                  <span class="badge badge-pill badge-success-soft mb-3">
                      <span class="h5 text-uppercase">dashboard</span>
                  </span>
                </div>
                  <!-- Heading -->
                  <h2 class="mt-2">
                      <span class="text-success">CHINCHIN</span> te permite Administrar todos tus Puntos de Venta en un solo Lugar!
                  </h2>
                  <h3 class="lead">¡Que nada se te escape!</h3>
                  <!-- Text -->
                  <p class="flead text-gray-700 mb-6 text-justify">
                     Nuestro software te brinda una solución eficaz para gestionar todos los Puntos de Venta de tus negocios en una sola plataforma.
                  </p>
                </div>
            </div> <!-- / .row -->
        </div> <!-- / .container -->
    </section>

    {{-- SECTION PUNTO DE VENTA --}}
    <section class="pt-8 py-md-11 pb-8">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                <!-- Badge -->
                <span class="badge badge-pill badge-success-soft mb-3">
                    <span class="h5 text-uppercase">punto de venta</span>
                </span>
                <!-- Heading -->
                <h2 class="h1 my-3">
                  Gestiona Todos tus Puntos de Venta <span class="text-success">CHINCHIN</span> con un solo usuario
                </h2>
                <!-- Text -->
                <p class="lead text-gray-700 mb-7 mb-md-9">Registra y Monitorea tus Ventas diarias por cada <span class="text-success">Punto de Venta</span>. Genera reportes en formatos sencillos para crear estrategias comerciales basadas en tu negocio.</p>
                </div>
                <div class="col-md-10" data-aos="fade-up">
                    <img src="{{asset('img/negocio/chinchin_negocio_punto_de_venta.png')}}" class="img-fluid">     
                </div> <!-- / .row -->
            </div> <!-- / .row -->
        </div> <!-- / .container -->
    </section>

    {{-- SECTION BITCOIN --}}
    <section class="py-8 py-md-11">
        <div class="container">
            <div class="row justify-content-center">
            <div class="col-12 text-center">
                <!-- Badge -->
                <span class="badge badge-pill badge-success-soft mb-3">
                <span class="h5 text-uppercase">bitcoin</span>
                </span>
                <!-- Heading -->
                <h2 class="h1">
                <span class="text-uppercase text-success">Venezuela</span> es el país número 1 en Latinoamérica en intercambios de <span class="text-success">Bitcoins</span>
                </h2>
                <!-- Text -->
                <p class="lead text-gray-700 mb-7 mb-md-9">No te quedes atrás y comienza a aceptar BTC, Cambialos a USD en un solo click</p>
            </div>
            </div> <!-- / .row -->
            <div class="row align-items-center">
            <div class="col-12 col-md-6 col-lg-7">
                <!-- Screenshot -->
                <div class="mb-8 mb-md-0">
                <!-- Image -->
                <img src="{{asset('img/negocio/chinchin_negocio_bitcoin.png')}}" alt="..." class="img-fluid mw-md-110 float-right mr-md-6 mb-6 mb-md-0">
                </div>
            </div>
            <div class="col-12 col-md-6 col-lg-5">
                <!-- List -->
                <div class="d-flex">
                <!-- Badge -->
                <div class="badge badge-lg badge-rounded-circle badge-success-soft mt-1">
                    <span>1</span>
                </div>
                <!-- Body -->
                <div class="ml-5">
                    <!-- Heading -->
                    <h3 class="text-uppercase">
                    Fácil y seguro
                    </h3>
                    <!-- Text -->
                    <p class="text-gray-700 mb-6">
                     Gracias a nuestra plataforma online
                    </p>
                </div>
                </div>
                <div class="d-flex">   
                <!-- Badge -->
                <div class="badge badge-lg badge-rounded-circle badge-success-soft mt-1">
                    <span>2</span>
                </div>
                <!-- Body -->
                <div class="ml-5">
                    <!-- Heading -->
                    <h3 class="text-uppercase">
                    Sin intermediarios
                    </h3>
                    <!-- Text -->
                    <p class="text-gray-700 mb-6">
                    Tu privacidad está garantizada
                    </p>
                </div>
                </div>
                <div class="d-flex">    
                <!-- Badge -->
                <div class="badge badge-lg badge-rounded-circle badge-success-soft mt-1">
                    <span>3</span>
                </div>
                <!-- Body -->
                <div class="ml-5">
                    <!-- Heading -->
                    <h3 class="text-uppercase">
                    Tus fondos a buen resguardo
                    </h3>
                    <!-- Text -->
                    <p class="text-gray-700 mb-0">
                    Siempre listos para que les des uso.

                    </p>
                </div>
                </div>
            </div>
            </div> <!-- / .row -->
        </div> <!-- / .container -->
    </section>

     {{-- SECTION PROCESO --}}
    <section class="pt-8 pt-md-11">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                    <!-- Badge -->
                    <span class="badge badge-pill badge-success-soft mb-3">
                        <span class="h5 text-uppercase">solución</span>
                    </span>
                    <!-- Heading -->
                    <h2 class="h1">
                        <span class="text-success">CHINCHIN</span> soluciona cualquier problema transaccional de nuestros clientes.
                    </h2>
                    <!-- Text -->
                    <p class="lead text-gray-700 mb-7 mb-md-9"> Incrementa las capacidades de tu negocio ofreciéndole opciones innovadoras de pago a tus clientes. <span class="text-success">CHINCHIN</span> te ahorra dolores de cabeza.</p>
                </div>
                <img src="{{asset('img/negocio/chinchin_negocio_proceso.png')}}" class="img-fluid"  data-aos="fade-up">     
            </div> <!-- / .row -->
            <div class="row py-8">
                <div class="col-12 col-md-4 text-center mb-6" data-aos="fade-up">
                    
                    <!-- Icon -->
                    <div class="icon text-primary mb-3">
                      <img width="80" src="{{asset('img/icons/chinchin/dinero_verde.png')}}" alt="icono 1">
                    </div>

                    <!-- Text -->
                    <p class="text-muted mb-6 mb-md-0">
                    Olvídate de los problemas asociados a darle el vuelto a tus clientes. 
                    </p>

                </div>
                <div class="col-12 col-md-4 text-center mb-6" data-aos="fade-up" data-aos-delay="50">

                    <!-- Icon -->
                    <div class="icon text-primary mb-3">
                      <img width="80" src="{{asset('img/icons/chinchin/punto_de_venta_verde.png')}}" alt="icono 1">
                    </div>
                    <!-- Text -->
                    <p class="text-muted mb-6 mb-md-0">
                     Adquiere tu Punto de Venta CHINCHIN a través de nuestros Bancos Aliados. 

                    </p>

                </div>
                <div class="col-12 col-md-4 text-center mb-6" data-aos="fade-up" data-aos-delay="100">
                    
                  <!-- Icon -->
                    <div class="icon text-primary mb-3">
                      <img width="80" src="{{asset('img/icons/chinchin/global_verde.png')}}" alt="icono 1">
                    </div>

                    <!-- Text -->
                    <p class="text-muted mb-0">
                        Adapta tu negocio a los medios de pago más innovadores y aceptados a nivel internacional.
                    </p>

                </div>
            </div> <!-- / .row -->
        </div> <!-- / .container -->
    </section>

    <section class="py-9 py-md-10">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-12 col-md-10 text-center">
            <!-- Badge -->
            <span class="badge badge-pill badge-success-soft mb-3">
                <span class="h5 text-uppercase">Dispositivo</span>
            </span>
            <!-- Heading -->
            <h2 class="h1">
              Registrar tu negocio en <span class="text-success text-uppercase">Chinchin</span> es muy sencillo.

            </h2>

            <!-- Text -->
            <p class="lead text-gray-700">
              Completa tu registro, verifícate y comienza a disfrutar de las bondades que solo <span class="text-success text-uppercase">CHINCHIN</span> puede ofrecerte.
            </p>
          </div>
        </div> <!-- / .row -->
        <div class="row align-items-center no-gutters">
          <div class="col-12 col-md-6">

            <!-- Card -->
            <div class="card rounded-lg shadow-lg mb-6 mb-md-0" style="z-index: 1;" data-aos="fade-up">

              <!-- Body -->
              <div class="card-body py-6 py-md-8">
                <div class="row justify-content-center">
                  <div class="col-12 col-xl-9">
                     <!-- Badge -->
                    <p class="text-center mb-8 mb-md-11">
                      <span class="badge badge-pill badge-success-soft">
                        <span class="h5 font-weight-bold text-uppercase">punto de venta</span>
                      </span>
                    </p>

                    <img class="p-0 mw-md-130" src="{{asset('img/negocio/chinchin_negocio_punto_de_venta_2.png')}}">
                
                  </div>
                </div> <!-- / .row -->
              </div>

              <!-- Button -->
              <a href="#!" class="card-btn btn btn-block btn-lg btn-success">
                Comprar
              </a>

            </div>

          </div>
          <div class="col-12 col-md-6 ml-md-n3">

            <!-- Card -->
            <div class="card rounded-lg shadow-lg" data-aos="fade-up" data-aos-delay="200">

              <!-- Body -->
              <div class="card-body py-6 py-md-8">
                <div class="row justify-content-center">
                  <div class="col-12 col-xl-10">

                        <!-- Badge -->
                    <div class="text-center mb-5">
                      <span class="badge badge-pill badge-success-soft">
                        <span class="h5 font-weight-bold text-uppercase">ventajas</span>
                      </span>
                    </div>

                    {{-- 
                    <div class="d-flex justify-content-center mb-6">
                      <span class="h2 mb-0 mt-2">$</span>
                      <span class="price display-2 mb-0" data-annual="29" data-monthly="49">299</span>
                    </div>
                    --}}

                    <!-- Features -->
                    <div class="d-flex mb-2">

                      <!-- Check -->
                      <div class="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                        <i class="fe fe-check"></i>
                      </div>

                      <!-- Text -->
                      <p class="font-size-lg">
                        Económico

                      </p>

                    </div>
                    <div class="d-flex mb-2">

                      <!-- Check -->
                      <div class="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                        <i class="fe fe-check"></i>
                      </div>

                      <!-- Text -->
                      <p class="font-size-lg">
                        Ligero
                      </p>

                    </div>
                    <div class="d-flex mb-2">

                      <!-- Check -->
                      <div class="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                        <i class="fe fe-check"></i>
                      </div>

                      <!-- Text -->
                      <p class="font-size-lg">
                      Pantalla táctil
                      </p>

                    </div>
                    <div class="d-flex mb-2">

                      <!-- Check -->
                      <div class="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                        <i class="fe fe-check"></i>
                      </div>

                      <!-- Text -->
                      <p class="font-size-lg">
                       Dispositivo android integrado
                      </p>

                    </div>
                    <div class="d-flex mb-2">

                      <!-- Check -->
                      <div class="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                        <i class="fe fe-check"></i>
                      </div>

                      <!-- Text -->
                      <p class="mb-0">
                        Materiales de calidad

                      </p>

                    </div>

                    <div class="d-flex mb-2">

                      <!-- Check -->
                      <div class="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                        <i class="fe fe-check"></i>
                      </div>

                      <!-- Text -->
                      <p class="font-size-lg">
                       Certificado Internacionalmente

                      </p>

                    </div>
                    <div class="d-flex mb-2">

                      <!-- Check -->
                      <div class="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                        <i class="fe fe-check"></i>
                      </div>

                      <!-- Text -->
                      <p class="font-size-lg">
                       Tecnologia de punta

                      </p>

                    </div>
                    <div class="d-flex mb-2">

                      <!-- Check -->
                      <div class="badge badge-rounded-circle badge-success-soft mt-1 mr-4">
                        <i class="fe fe-check"></i>
                      </div>

                      <!-- Text -->
                      <p class="font-size-lg">
                       Confiable y seguro
                      </p>

                    </div>
                  </div>
                </div> <!-- / .row -->
              </div>

              <!-- Button -->
              <a href="#!" class="card-btn btn btn-block btn-lg btn-light bg-gray-300 text-gray-700">
                Saber más
              </a>

            </div>

          </div>
        </div> <!-- / .row -->
      </div> <!-- / .container -->
    </section>

   @include('partials.sections.clientes-verde')
   
@endsection