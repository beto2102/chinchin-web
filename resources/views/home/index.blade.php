@extends('layouts.website')

@section('title', 'ChinChin')

@section('hero-banner')

<section>
      <div class="flickity-button-bottom" data-flickity='{"autoPlay": true,"imagesLoaded": true, "wrapAround": true, "pageDots": false}'>
        <div class="w-100">
          <div class="container">
            <div class="row align-items-center text-center text-md-left">
              <div class="col-12 col-md-6 offset-md-1 order-md-2">
                <img src="{{asset('img/home/chinchin_home_hero_slide_1.png')}}" alt="..." class="img-fluid mw-md-110" data-aos="fade-left">
              </div>
              <div class="col-12 col-md-5 py-8 py-md-14 order-md-1 text-md-left text-center" data-aos="fade-right">
                <!-- Heading -->
                <h1 class="display-4">
                Te Damos el <span class="text-success">Poder</span>
                </h1>
                <!-- Text -->
                <p class="lead text-justify text-muted mb-6 mb-md-8">
                de controlar y usar tu dinero a tu estilo, en una plataforma de pagos sin fronteras. Compra, ahorra, intercambia y gasta tus monedas digitales o fiduciarias en cualquier momento y en cualquier lugar.
                </p> 
                <!-- Button -->
                <button type="submit" class="btn btn-block col text-uppercase btn-success shadow lift">
                REGÍSTRATE
                </button>
              </div>
            </div>
          </div>  
        </div>
        <div class="w-100">
          <div class="container">
            <div class="row align-items-center text-center text-md-left">
              <div class="col-12 col-md-6 offset-md-1 order-md-2">
                <img src="{{asset('img/home/chinchin_home_hero_slide_2.png')}}" alt="..." class="img-fluid mw-md-110">
              </div>
              <div class="col-12 col-md-5 py-8 py-md-14 order-md-1 text-md-left text-center" data-aos="fade-right">
                <!-- Heading -->
                <h1 class="display-4">
                <span class="text-success text-uppercase">CHINCHIN</span> <br/>Brinda soluciones <span class="text-uppercase">eficientes y seguras</span>
                </h1>
                <!-- Text -->
                <p class="lead text-justify text-muted mb-6 mb-md-8">
                 a las necesidades financieras de todos los clientes que confían en nosotros y se han unido a nuestro ecosistema digital.
                </p> 
                <!-- Button -->
                <button type="submit" class="btn btn-block col text-uppercase btn-success shadow lift">
                REGÍSTRATE
                </button>
              </div>
            </div>
          </div>  
        </div>
         <div class="w-100">
          <div class="container">
            <div class="row align-items-center text-center text-md-left">
              <div class="col-12 col-md-6 offset-md-1 order-md-2">
                <img src="{{asset('img/home/chinchin_home_hero_slide_3.png')}}" alt="..." class="img-fluid mw-md-110">
              </div>
              <div class="col-12 col-md-5 py-8 py-md-14 order-md-1 text-md-left text-center" data-aos="fade-right">
                <!-- Heading -->
                <h1 class="display-4">
                 Punto de Venta <span class="text-success">MULTIMONEDA</span>,
                </h1>
                <!-- Text -->
                <p class="lead text-justify text-muted mb-6 mb-md-8">
                 inalámbrico, portable, pantalla táctil configurable y monitoreable en tiempo real. Si no lo usas, no te cobramos nada, ¡Comisiones bajas!
                </p> 
                <!-- Button -->
                <button type="submit" class="btn btn-block col text-uppercase btn-success shadow lift">
                VER Más
                </button>
              </div>
            </div>
          </div>  
        </div>
      </div>
    </section>
@endsection

@section('content')

  {{-- SECTION PROCESO --}}
  <section class="pt-8 pt-md-11">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-12 col-md-10 text-center">
          <!-- Badge -->
          <span class="badge badge-pill badge-success-soft mb-3">
            <span class="h5 text-uppercase">registro</span>
          </span>
          <!-- Heading -->
          <h1>
            ¡Con <span class="text-success text-uppercase">CHINCHIN</span> te la ponemos fácil!
          </h1>
          <!-- Text -->
          <p class="lead text-justify text-md-center text-gray-700 mb-7 mb-md-9">Estamos construyendo la plataforma financiera más completa de Venezuela para realizar transacciones de manera simple y segura.</p>
        </div>
      </div> <!-- / .row -->
      <div class="row align-items-center">
        <div class="col-12 col-md-6 col-lg-7">
          <!-- Screenshot -->
          <div class="mb-8 mb-md-0">
            <!-- Image -->
            <img src="{{asset('img/home/chinchin_home_proceso_vida_simple.png')}}" alt="..." class="screenshot img-fluid mw-md-110 float-right mr-md-6 mb-6 mb-md-0">
          </div>
        </div>
        <div class="col-12 col-md-6 col-lg-5 mt-4">
          <!-- List -->
          <div class="d-flex mb-8">
            <!-- Badge -->
            <div class="badge badge-lg badge-rounded-circle badge-success-soft mt-1">
              <span>1</span>
            </div>
            <!-- Body -->
            <div class="ml-5">
              <!-- Heading -->
              <h3 class="lead">
                Registrate en nuestra Página Web 
              </h3>
              
            </div>
          </div>
          <div class="d-flex mb-8">   
            <!-- Badge -->
            <div class="badge badge-lg badge-rounded-circle badge-success-soft mt-1">
              <span>2</span>
            </div>
            <!-- Body -->
            <div class="ml-5">
              <!-- Heading -->
              <h3 class="lead">
                Conviértete en un Usuario Verificado
              </h3>
              <!-- Text -->
             
            </div>
          </div>
          <div class="d-flex mb-8">    
            <!-- Badge -->
            <div class="badge badge-lg badge-rounded-circle badge-success-soft mt-1">
              <span>3</span>
            </div>
            <!-- Body -->
            <div class="ml-5">
              <!-- Heading -->
              <h3 class="lead">
                Comienza a Pagar usando <span class="text-success">CHINCHIN</span>.
              </h3>
              <!-- Text -->
              
            </div>
          </div>
        </div>
      </div> <!-- / .row -->
    </div> <!-- / .container -->
  </section>

  {{-- SECTION SOLUCIONES --}}
  <section class="pt-8 pt-md-11">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-12 col-lg-10 text-center">     
          <!-- Badge -->
          <span class="badge badge-pill badge-success-soft mb-3">
            <span class="h5 text-uppercase">soluciones</span>
          </span>
          <!-- Heading -->
          <h1>
            <span class="text-success text-uppercase font-weight-bold">CHINCHIN</span> te ofrece herramientas tecnológicas que se adaptan a tus necesidades
          </h1>
          <!-- Text -->
          <p class="font-size-lg text-gray-700 mb-7 mb-md-9">
            Disfruta de la libertad de manejar tu dinero sin intermediario con las diferentes soluciones que <span class="text-uppercase">chinchin</span> te ofrece.
          </p>
        </div>
      </div> <!-- / .row -->
      <div class="row align-items-center">
        <div class="col-md-6">
            <img src="{{asset('img/home/chinchin_home_soluciones.png')}}" class="img-fluid" alt="..."> 
        </div>
        <div class="col-md-6 mt-6">
            <!-- List -->
          <div class="d-flex mb-6">
            <!-- Icon -->
            <div class="icon text-primary">
              <img width="40" src="{{asset('img/icons/chinchin/bank.png')}}" alt="logo-solucion-1">
            </div>
            <div class="ml-5">
              <!-- Heading -->
              <h4>
                Transferencias bancarias nacionales, depósitos y retiros en efectivo de USD y EUR.
              </h4>
            </div>
          </div>
          <div class="d-flex mb-6">
            <!-- Icon -->
            <div class="icon text-primary">
              <img width="40" src="{{asset('img/icons/chinchin/crypto.png')}}" alt="logo-solucion-1">
            </div>
            <div class="ml-5">
              <!-- Heading -->
              <h4>
                Recibe pagos en Criptomonedas y conviértelos fácilmente en dólares.
              </h4>
            </div>
          </div>
          <div class="d-flex mb-6">
            <!-- Icon -->
            <div class="icon text-primary">
              <img width="40" src="{{asset('img/icons/chinchin/punto_de_venta.png')}}" alt="logo-solucion-1">
            </div>
            <div class="ml-5">
              <!-- Heading -->
              <h4>
                Paga con USD, EUR, VES y BTC en los Puntos de Venta CHINCHIN.
              </h4>
            </div>
          </div>
          <div class="d-flex mb-6">
            <!-- Icon -->
            <div class="icon text-primary">
              <img width="40" src="{{asset('img/icons/chinchin/transferencia.png')}}" alt="logo-solucion-1">
            </div>
            <div class="ml-5">
              <!-- Heading -->
              <h4>
                Transferencias Internas entre Usuarios.
              </h4>
            </div>
          </div>
          <div class="d-flex mb-6">
            <!-- Icon -->
            <div class="icon text-primary">
              <img width="40" src="{{asset('img/icons/chinchin/pago_movil.png')}}" alt="logo-solucion-1">
            </div>
            <div class="ml-5">
              <!-- Heading -->
              <h4>
                Envía y recibe pagos a través de Pago Móvil de forma automática.
              </h4>
            </div>
          </div>
          <div class="d-flex mb-6">
            <!-- Icon -->
            <div class="icon text-primary">
              <img width="40" src="{{asset('img/icons/chinchin/sms.png')}}" alt="logo-solucion-1">
            </div>
            <div class="ml-5">
              <!-- Heading -->
              <h4>Consulta, transfiere y paga con SMS, sin la necesidad de un teléfono inteligente.</h4>
            </div>
          </div>
          <div class="d-flex mb-6">
            <!-- Icon -->
            <div class="icon text-primary">
              <img width="40" src="{{asset('img/icons/chinchin/tarjeta_prepago.png')}}" alt="logo-solucion-1">
            </div>
            <div class="ml-5">
              <!-- Heading -->
              <h4>
                 Obtén tu Tarjeta Prepagada con límites personalizables.
              </h4>
            </div>
          </div>
          <div class="d-flex mb-6">
            <!-- Icon -->
            <div class="icon text-primary">
              <img width="40" src="{{asset('img/icons/chinchin/tarjeta_prepago.png')}}" alt="logo-solucion-1">
            </div>
            <div class="ml-5">
              <!-- Heading -->
              <h4>
                  Envía y recibe pagos a través de Zelle.
              </h4>
            </div>
          </div>

        </div>
      </div>
    </div> <!-- / .container -->
  </section>

  {{-- SECTION SERVICIO EN DOLARES --}}
  <section class="pt-8 pt-md-11">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12 col-md-7 col-lg-6 position-relative order-2 order-md-1" data-aos="fade-right">
          <!-- Heading -->
          <h2 class="h1 mt-6">
           Paga en Efectivo
          </h2>
          <!-- Text -->
          <p class="font-size-lg text-muted mb-6">
            Deposita Dólares y Euros en tu cuenta <span class="text-uppercase text-success">chinchin</span> y comienza a pagar
          </p>
          <!-- List -->
          <div class="d-flex mb-6">          
            <!-- Badge -->
            <div class="badge badge-lg badge-rounded-circle badge-success-soft mt-1">
              <span>1</span>
            </div>           
            <!-- Body -->
            <div class="ml-5">           
              <!-- Heading -->
              <h3>
                Olvídate de los problemas asociados al vuelto.
              </h3>
            </div>
          </div>
          <div class="d-flex mb-6">
            <!-- Badge -->
            <div class="badge badge-lg badge-rounded-circle badge-success-soft mt-1">
              <span>2</span>
            </div> 
            <!-- Body -->
            <div class="ml-5">
              <!-- Heading -->
              <h3>
                Pagar con efectivo ahora será mucho más fácil.
              </h3>
            </div>
          </div>
          <div class="d-flex mb-6">  
            <!-- Badge -->
            <div class="badge badge-lg badge-rounded-circle badge-success-soft mt-1">
              <span>3</span>
            </div>
            <!-- Body -->
            <div class="ml-5">  
              <!-- Heading -->
              <h3>
                 Sin importar tu necesidad, <span class="text-uppercase text-success">chinchin</span> tiene una solución para ti.
              </h3>
            </div>
          </div>
        </div>
        <div class="col-12 col-md-5 col-lg-6 position-relative order-1 order-md-2">
          <div class="w-md-130 w-lg-120" data-aos="fade-left">
              <!-- Image -->
              <img src="{{asset('img/home/chinchin_home_servicios_dolares.png')}}" class="img-fluid" alt="...">
          </div>
        </div>
      </div> <!-- / .row -->
    </div> <!-- / .container -->
  </section>

  <section class="py-8 py-md-11">
      <div class="container">
            <!-- / .row -->
          <div class="row align-items-center">
          <div class="col-12 col-md-6 col-lg-7">
              <!-- Screenshot -->
              <div class="mb-8 mb-md-0">
              <!-- Image -->
              <img src="{{asset('img/home/chinchin_home_servicios_tarjeta_debito.png')}}" alt="..." class="img-fluid mw-md-110 float-right mr-md-6 mb-6 mb-md-0" data-aos="fade-right">
              </div>
          </div>
          <div class="col-12 col-md-6 col-lg-5" data-aos="fade-left">
                <!-- Heading -->
        <h2 class="h1">
          Tarjeta de Débito
        </h2>

        <!-- Text -->
        <p class="font-size-lg text-muted mb-6">
          Gracias a nuestros aliados comerciales disfrutarás de una alternativa de pago flexible.
        </p>
              <!-- List -->
        <div class="d-flex mb-6">          
          <!-- Badge -->
          <div class="badge badge-lg badge-rounded-circle badge-success-soft mt-1">
            <span>1</span>
          </div>
          
          <!-- Body -->
          <div class="ml-5">
            
            <!-- Heading -->
            <h3>
                Es aceptada en millones de comercios en toda Venezuela
            </h3>

          </div>

        </div>
        <div class="d-flex mb-6">
            
          <!-- Badge -->
          <div class="badge badge-lg badge-rounded-circle badge-success-soft mt-1">
            <span>2</span>
          </div>
          
          <!-- Body -->
          <div class="ml-5">
            <!-- Heading -->
            <h3>
              Es segura y conveniente, podras realizar tus compras y pagar en cualquier lugar.
            </h3>
          </div>
        </div>
        <div class="d-flex mb-6">
            
          <!-- Badge -->
          <div class="badge badge-lg badge-rounded-circle badge-success-soft mt-1">
            <span>3</span>
          </div>
          
          <!-- Body -->
          <div class="ml-5">
            
            <!-- Heading -->
            <h3>
                Recibe tu sueldo en cualquier divisa y usa tu dinero como gustes
            </h3>

          </div>

        </div>
              
        
          </div> <!-- / .row -->
      </div> <!-- / .container -->
  </section>

  {{-- SECTION SERVICIO MENSAJERIA DE TEXTO --}}
  <section class="pt-8 pt-md-11">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12 col-md-7 col-lg-6 position-relative order-2 order-md-1" data-aos="fade-right">
          <!-- Heading -->
          <h2 class="h1">
            Paga y Transfiere con <span class="text-uppercase text-success">solo</span> un Mensaje de Texto
          </h2>
          <!-- Text -->
          <p class="font-size-lg text-justify text-muted mb-6">
            Ahora puedes enviar, recibir, pagar y consultar tu saldo en tiempo real, solo con un SMS y sin la necesidad de un teléfono inteligente.
          </p>
          <!-- List -->
          <div class="d-flex mb-6">          
            <!-- Badge -->
            <div class="badge badge-lg badge-rounded-circle badge-success-soft mt-1">
              <span>1</span>
            </div>   
            <!-- Body -->
            <div class="ml-5">  
              <!-- Heading -->
              <h3>
               Realiza y recibe pagos las 24 horas del día y desde cualquier lugar.
              </h3>
            </div>
          </div>
          <div class="d-flex mb-6">
            <!-- Badge -->
            <div class="badge badge-lg badge-rounded-circle badge-success-soft mt-1">
              <span>2</span>
            </div>
            <!-- Body -->
            <div class="ml-5">
              <!-- Heading -->
              <h3>
               Solo necesitarás una línea telefónica activa.
              </h3>
            </div>
          </div>
          <div class="d-flex mb-6">       
            <!-- Badge -->
            <div class="badge badge-lg badge-rounded-circle badge-success-soft mt-1">
              <span>3</span>
            </div>
            <!-- Body -->
            <div class="ml-5">
              <!-- Heading -->
              <h3>
                 Usa tu dinero desde tu celular al instante.

              </h3>
            </div>
          </div>
        </div>
        <div class="col-12 col-md-5 col-lg-6 position-relative order-1 order-md-2">
          <div class="w-md-150 w-lg-130" data-aos="fade-left">
            <!-- Image -->
            <img src="{{asset('img/home/chinchin_home_servicios_mensajeria_de_texto.png')}}" class="img-fluid" alt="...">
          </div>
        </div>
      </div> <!-- / .row -->
    </div> <!-- / .container -->
  </section>

  {{-- SHAPE --}}
  <div class="position-relative mt-n8">
    <div class="shape shape-bottom shape-fluid-x svg-shim text-gray-200">
      <svg viewBox="0 0 2880 480" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M2160 0C1440 240 720 240 720 240H0v240h2880V0h-720z" fill="currentColor"/></svg>
    </div>
  </div>

  {{-- TESTIMONIALS --}}
  <section class="py-10 pt-md-12 bg-gray-200">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-12 col-md-10 text-center">    
          <!-- Badge -->
          <span class="badge badge-pill badge-success-soft mb-3">
            <span class="h5 text-uppercase">solución</span>
          </span> 
          <!-- Heading -->
          <h2>
            En <span class="text-success">CHINCHIN</span> te brindamos toda la ayuda que necesites para alcanzar tus metas.
          </h2>
        </div>
      </div> <!-- / .row -->
      <div class="row">
        <div class="col-12">
          <!-- Card -->
          <div class="chinchin-arrow card card-row shadow-light-lg mb-6">
            <div class="row no-gutters">
              <div class="col-12 col-md-6">
                <!-- Slider -->
                <div class="card-img-slider" data-flickity='{"fade": true, "imagesLoaded": true, "pageDots": false, "prevNextButtons": false, "asNavFor": "#blogSlider", "draggable": false}'>
                  <a class="card-img-left bg-cover" style="background-image: url({{asset('img/home/chinchin_home_testimonios_1.png')}});" href="#!">
                    <!-- Image (placeholder) -->
                  </a>
                </div>
                <!-- Shape -->
                <div class="shape shape-right shape-fluid-y svg-shim text-white d-none d-md-block">
                  <svg viewBox="0 0 112 690" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M116 0H51v172C76 384 0 517 0 517v173h116V0z" fill="currentColor"/></svg>
                </div>
              </div>
              <div class="col-12 col-md-6 position-md-static">
                <!-- Slider -->
                <div class="position-md-static" data-flickity='{"wrapAround": true, "pageDots": false, "adaptiveHeight": true}' id="blogSlider">
                  <div class="w-100">
                    <!-- Body -->
                    <div class="card-body">
                      <blockquote class="blockquote text-center mb-0">
                        <!-- Text -->
                        <p class="mb-5 mb-md-7">
                          “Conectamos a millones de personas y comercios a través de soluciones transaccionales que mejoran sus vidas.”
                        </p>
                      </blockquote>
                    </div>
                  </div>
                </div>
              </div>
            </div> <!-- / .row -->
          </div>
        </div>
      </div> <!-- / .row -->
    </div> <!-- / .container -->
  </section>

  {{-- BRANDS --}}
   @include('partials.sections.clientes-gris')
   @include('partials.sections.clientes-verde')
  

@endsection