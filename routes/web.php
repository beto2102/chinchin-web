<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * PAGINAS WEBS
 */

Route::get('/', function () {
    return view('home.index');
})->name("home");

Route::get('/usuarios', function () {
    return view('home.personas');
})->name("usuarios");

Route::get('/comercios', function () {
    return view('home.negocio');
})->name("comercios");

Route::get('/contacto', function () {
    return view('home.contacto');
})->name("contacto");

Route::get('/nosotros', function () {
    return view('home.nosotros');
})->name("nosotros");

Route::get('/terminos', function () {
    return view('home.terminos');
})->name("terminos");
